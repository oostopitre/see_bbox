# Web app demo for Vizualizing Bounding Box Annoation

Web app: [https://see-bbox.herokuapp.com/](https://see-bbox.herokuapp.com/)

When working on Computer Vision datasets, bounding box annotations often come in various formats. 
So this app lets you visualize a bounding box on an image in some of these typical annotation styles.
I found this to be useful tool to have during a quick debugging when things don't work. 
This will make sure you are not feeding garbage annotations during training.

![Alt-Text](assets/demo.png)


## Setup and run on localhost

```bash
# clone
$ git clone https://oostopitre@bitbucket.org/oostopitre/see_bbox.git
$ cd see_bbox

# create env
$ conda create -n see_bbox python=3.7
$ conda activate see_bbox
(see_bbox) $ pip install -r requirements.txt

# run demo
(see_bbox) $ streamlit run app/app.py
```


## Deployment

```bash
# setup
(see_bbox) $ brew tap heroku/brew && brew install heroku
(see_bbox) $ heroku login
(see_bbox) $ heroku git:remote -a see-bbox
# deploy
(see_bbox) $ git push heroku master
```

#### Credits

Inspired by Vladimir Iglovikov's Retinaface demo repo @ https://github.com/ternaus/retinaface_demo