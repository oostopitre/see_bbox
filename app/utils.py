import cv2
import numpy as np


def viz_annotation(
    image: np.ndarray,
    bbox_tl_x: int,
    bbox_tl_y: int,
    bbox_br_x: int,
    bbox_br_y: int,
) -> np.ndarray:

    vis_image = image.copy()

    vis_image = cv2.rectangle(
        image,
        (bbox_tl_x, bbox_tl_y),
        (bbox_br_x, bbox_br_y),
        color=(0, 255, 0),
        thickness=2,
    )
    return vis_image


def parse_coords(
    coord_style: str,
    coord_values: str,
    image_width: int,
    image_height: int,
):

    try:
        if coord_style == 'x_min, y_min, x_max, y_max':
            x_min, y_min, x_max, y_max = map(int, coord_values.split(','))

        elif coord_style == 'c_x, c_y, width, height':
            c_x, c_y, width, height = map(int, coord_values.split(','))
            x_min = int(c_x - width / 2)
            y_min = int(c_y - height / 2)
            x_max = int(c_x + width / 2)
            y_max = int(c_y + height / 2)

        elif coord_style == 'c_x_per, c_y_per, width_per, height_per':
            c_x, c_y, width, height = map(float, coord_values.split(','))
            x_min = int((c_x - width / 2) * image_width)
            y_min = int((c_y - height / 2) * image_height)
            x_max = int((c_x + width / 2) * image_width)
            y_max = int((c_y + height / 2) * image_height)

        return x_min, y_min, x_max, y_max

    except ValueError:
        return None, None, None, None
