import numpy as np
import streamlit as st
from PIL import Image

import utils

st.set_option('deprecation.showfileUploaderEncoding', False)

ANNO_OPTIONS = [
    'x_min, y_min, x_max, y_max',
    'c_x, c_y, width, height',
    'c_x_per, c_y_per, width_per, height_per',
]

# Interface
st.title('Visualize BBox Coords on an Image')
uploaded_file = st.sidebar.file_uploader('Upload an Image...', type='jpg')

if uploaded_file is not None:
    image = np.array(Image.open(uploaded_file))
    image_height, image_width, _ = image.shape

    st.image(
        image,
        caption=f'Before: Width = {image_width}, Height = {image_height}',
        use_column_width=True
    )
    st.write('')
    st.write('Plotting bounding box...')

    # define coord annotation styles
    coord_style = st.sidebar.radio(
        label='Pick Annotation Style',
        index=0,
        options=ANNO_OPTIONS,
    )
    coord_values = st.sidebar.text_input('Input coords - Eg: 10,10,200,300', value='')

    # Display bounding boxes
    if not coord_style or coord_values == '':
        st.write('Pick a style and coord values to visualize...')
    else:
        x_min, y_min, x_max, y_max = utils.parse_coords(
            coord_style,
            coord_values,
            image_width,
            image_height,
        )
        if x_min and x_max and y_min and y_max:
            visualized_image = utils.viz_annotation(image, x_min, y_min, x_max, y_max)
            st.image(visualized_image, caption='After', use_column_width=True)
        else:
            st.markdown('<font color="red">INVALID INPUTS</font>', unsafe_allow_html=True)
